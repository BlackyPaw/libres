/*
 * main.cpp
 *
 *  Created on: 20.07.2014
 *      Author: euaconlabs
 */

#include "resfile.h"

#include <cstring>
#include <fstream>

int main(int argc, char* argv[])
{
	rsFileWriteInfo files[1];

	files[0].flags = rsFILE_COMPRESSED;
	std::strncpy(files[0].name, "Textures.screenshot", 256);
	std::strncpy(files[0].file, "screenshot.png", 256);

	rsResourceFile::WriteFile("respack.res", files, 1);

	rsResourceFile resfile;
	if(!resfile.Load("respack.res"))
		return 1;

	rsFileInfo* screenshot = resfile.OpenFile("Textures.screenshot");
	if(screenshot == 0)
		return 2;

	std::ofstream out("screenshot2.png", std::ios::binary | std::ios::out);
	out.write((const char*)screenshot->GetData(), screenshot->GetSize());
	out.close();

	return 0;
}
