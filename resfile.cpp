/*
 * resfile.cpp
 *
 *  Created on: 19.07.2014
 *      Author: euaconlabs
 */

#include "resfile.h"
#include "zlib/zlib.h"

#include <cassert>
#include <cstdio>
#include <cstring>
#include <vector>

rsResourceFile::rsResourceFile() :
	native(false),
	fileChunk(0),
	fileTable(0)
{
}

rsResourceFile::~rsResourceFile()
{
	if(input)
		input.close();

	if(fileTable != 0)
		delete[] fileTable;
}

bool rsResourceFile::WriteFile(const char* file, const rsFileWriteInfo* files, const rsu32_t numFiles)
{
	std::ofstream out(file, std::ios::binary | std::ios::out);
	if(!out)
		return false;

	// - Write the base header:
	rsBaseHeader baseHeader =
	{
		{ 'R', 'E', 'S', ' ' },
		rsFileEndianness,
		{ 1, 0 },
		numFiles,
		0
	};

	out.write((char*)&baseHeader, sizeof(baseHeader));

	rsu32_t fileChunk = out.tellp();
	rsFileTableEntry* entries = new rsFileTableEntry[numFiles];

	// - Write all files:
	rsWriteState state =
	{
		out,
		fileChunk,
		&files[0],
		&entries[0]
	};

	for(rsu32_t i = 0; i < numFiles; ++i)
	{
		state.file = &files[i];
		state.entry = &entries[i];

		if(files[i].flags & rsFILE_COMPRESSED)
		{
			if(!WriteFileCompressed(state))
			{
				delete[] entries;
				return false;
			}
		}
		else
		{
			if(!WriteFileRaw(state))
			{
				delete[] entries;
				return false;
			}
		}
	}

	rsu32_t fileTableStart = out.tellp();

	// - Write file table:
	for(rsu32_t i = 0; i < numFiles; ++i)
	{
		// - Write table entry:
		out.write((char*)&entries[i], sizeof(rsFileTableEntry));

		// - Write the file name:
		out.write((char*)files[i].name, entries[i].namelen);
	}

	rsu32_t fileTableEnd = out.tellp();

	// - Write the file table record:
	rsFileTableRecord record;
	record.size = fileTableEnd - fileTableStart;
	out.write((char*)&record, sizeof(record));

	delete[] entries;
	out.close();

	return true;
}

bool rsResourceFile::Load(const char* file)
{
	if(input)
		input.close();

	input.open(file, std::ios::binary | std::ios::in);

	// - Read the file header:
	input.read((char*)&fileHeader, sizeof(fileHeader));

	if(std::strncmp(fileHeader.signature, "RES ", 4) != 0)
	{
		input.close();
		return false;
	}

	// - Check endianness:
	native = (*((char*)(&fileHeader.endian)) == *((char*)(&rsFileEndianness)));
	if(!native)
		rsSwap(fileHeader);

	// - Goto start of the file chunk:
	input.seekg(fileHeader.offset, std::ios::cur);

	// - Store the position of the file chunk:
	fileChunk = input.tellg();

	// - Now load the file entry table:
	rsFileTableRecord table;
	input.seekg(-sizeof(table), std::ios::end);

	input.read((char*)&table, sizeof(table));
	if(!native)
		rsSwap(table);

	input.seekg(-((rss32_t)(table.size)) - sizeof(table), std::ios::end);

	// - Read all file table entries:
	if(fileTable != 0)
		delete[] fileTable;

	fileTable = new rsFileTableEntryMem[fileHeader.files];

	for(rsu32_t i = 0; i < fileHeader.files; ++i)
	{
		// - Read a single entry:
		rsFileTableEntry entry;
		input.read((char*)&entry, sizeof(entry));
		if(!native)
			rsSwap(entry);

		// - Store in memory:
		fileTable[i].offset = entry.offset;
		input.read(fileTable[i].name, entry.namelen);

		// - Insert '\0' at the end, if necessary:
		if(entry.namelen < 255)
			fileTable[i].name[entry.namelen] = '\0';
	}

	return true;
}

rsFileInfo* rsResourceFile::OpenFile(const char* file)
{
	// - Traverse our file table:
	for(rsu32_t i = 0; i < fileHeader.files; ++i)
	{
		if(std::strncmp(fileTable[i].name, file, RS_FILE_MAX_NAME_LEN) == 0)
		{
			// - Load our file info:
			rsFileHeader local;
			input.seekg(fileChunk + fileTable[i].offset, std::ios::beg);
			input.read((char*)&local, sizeof(local));
			if(!native)
				rsSwap(local);

			rsFileInfo* info = 0;

			if(local.flags & rsFILE_COMPRESSED)
			{
				info = OpenFileCompressed(local);
				if(info == 0)
					return 0;
			}
			else
			{
				info = OpenFileRaw(local);
				if(info == 0)
					return 0;
			}
			std::strncpy(info->name, fileTable[i].name, RS_FILE_MAX_NAME_LEN);

			// - Return the new file info:
			return info;
		}
	}

	return 0;
}

bool rsResourceFile::WriteFileRaw(rsWriteState& state)
{
	// - Open input file:
	std::ifstream in(state.file->file, std::ios::binary | std::ios::in);
	if(!in)
		return false;

	rsFileHeader local;
	state.entry->offset = (rsu32_t)(state.out.tellp()) - state.filechunk;
	state.entry->namelen = std::strlen(state.file->name);

	// - Determine file size:
	in.seekg(0, std::ios::end);
	local.size = in.tellg();
	in.seekg(0, std::ios::beg);

	// - Set flags:
	local.flags = state.file->flags;
	local.rawsize = 0;

	// - Write header:
	state.out.write((char*)&local, sizeof(local));

	// - Copy data:
	rsu8_t* data = new rsu8_t[local.size];
	in.read((char*)data, local.size);
	state.out.write((char*)data, local.size);
	delete[] data;

	in.close();
	return true;
}

bool rsResourceFile::WriteFileCompressed(rsWriteState& state)
{
	// - Open input file:
	std::ifstream in(state.file->file, std::ios::binary | std::ios::in);
	if(!in)
		return false;
	in.clear();

	rsFileHeader local;
	state.entry->offset = (rsu32_t)(state.out.tellp()) - state.filechunk;
	state.entry->namelen = std::strlen(state.file->name);

	// - Determine raw file size:
	in.seekg(0, std::ios::end);
	local.rawsize = in.tellg();
	in.seekg(0, std::ios::beg);

	// - Set flags:
	local.flags = state.file->flags;
	local.size  = 0;


	// - Leave room for the file header:
	state.out.seekp(sizeof(local), std::ios::cur);
	std::streamoff header_pos = state.out.tellp();

	// - Now compress using zlib:

	// COMPRESSION
	{

		z_stream zstr;
		zstr.zalloc = Z_NULL;
		zstr.zfree	= Z_NULL;
		zstr.opaque	= Z_NULL;

		rsu8_t* inb  = new rsu8_t[16384*2];
		rsu8_t* outb = &inb[16384];

		rss32_t ret, flush;
		rsu32_t have;
		ret = deflateInit(&zstr, 6);
		if(ret != Z_OK)
		{
			delete[] inb;
			return false;
		}

		do
		{
			zstr.avail_in = in.read((char*)inb, 16384).gcount();
			// Check for errors:
			if(in.bad())
			{
				deflateEnd(&zstr);
				delete[] inb;
				return false;
			}

			// Check for End-of-File:
			flush = in.eof() ? Z_FINISH : Z_NO_FLUSH;
			zstr.next_in = inb;

			do
			{
				zstr.avail_out = 16384;
				zstr.next_out  = outb;

				ret = deflate(&zstr, flush);
				// If you ever come to this error it will mean that you have overridden
				// internally kept memory of zlib. Bad boy!
				assert(ret != Z_STREAM_ERROR);

				have = 16384 - zstr.avail_out;
				local.size += have;

				// Write directly to our output file:
				state.out.write((char*)outb, have);

			} while(zstr.avail_out == 0 && (flush != Z_FINISH || ret == Z_STREAM_END));

			// Once we've been flushing the stream and then got out of the inner do-loop we're done!
		} while(flush != Z_FINISH);

		// Cleanup:
		deflateEnd(&zstr);
		delete[] inb;
	}

	std::streamoff file_pos = state.out.tellp();

	// - Now we can go back to write our file header:
	state.out.seekp(header_pos, std::ios::beg);

	// - Write the file header:
	state.out.write((char*)&local, sizeof(local));

	// - And last but not least advance the stream in order to make it ready for the next file:
	state.out.seekp(file_pos, std::ios::beg);

	in.close();
	return true;
}

rsFileInfo* rsResourceFile::OpenFileRaw(const rsFileHeader& header)
{
	rsFileInfo* info = new rsFileInfo();
	info->size = header.size;
	info->flags = header.flags;

	// - Read the file's contents:
	info->data = new rsu8_t[info->size];
	input.read((char*)info->data, info->size);

	return info;
}

rsFileInfo* rsResourceFile::OpenFileCompressed(const rsFileHeader& header)
{
	rsFileInfo* info = new rsFileInfo();
	info->size = header.rawsize;
	info->flags = header.flags;

	// - Read the compressed data:
	rsu8_t* compressed = new rsu8_t[header.size];
	input.read((char*)compressed, header.size);

	info->data = new rsu8_t[info->size];

	// DECOMPRESSION
	{
		rss32_t ret;
		z_stream zstr;

		// - Set up the z_stream:
		zstr.zalloc = Z_NULL;
		zstr.zfree 	= Z_NULL;
		zstr.opaque = Z_NULL;

		// - Set up input parameters and initialize state:
		zstr.avail_in = 0;
		zstr.next_in  = Z_NULL;

		ret = inflateInit(&zstr);
		if(ret != Z_OK)
		{
			delete[] compressed;
			delete	 info;
			return 0;
		}

		zstr.avail_in = header.size;
		zstr.next_in  = compressed;

		zstr.avail_out = info->size;
		zstr.next_out  = info->data;

		ret = inflate(&zstr, Z_NO_FLUSH);
		// You've clobbered zlib's internally kept memory, you bad boy!
		assert(ret != Z_STREAM_ERROR);

		if(ret == Z_NEED_DICT)
			ret = Z_DATA_ERROR;

		switch(ret)
		{
			case Z_DATA_ERROR:
			case Z_MEM_ERROR:
				inflateEnd(&zstr);
				delete[] compressed;
				delete	 info;
				return 0;
		}

		// - Adjust size parameter if necessary. Should usually be the same as before:
		info->size = info->size - zstr.avail_out;

		// - Cleanup:
		inflateEnd(&zstr);
	}

	delete[] compressed;
	return info;
}
