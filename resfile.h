/*
 * resfile.h
 *
 *  Created on: 19.07.2014
 *      Author: euaconlabs
 */

#ifndef RESFILE_H_
#define RESFILE_H_

#include "fileinfo.h"
#include "headers.h"

#include <fstream>

enum rsFileFlags
{
	rsFILE_RAW			= 0x00,
	rsFILE_COMPRESSED 	= 0x01
};

class rsResourceFile
{
public:

	rsResourceFile();
	~rsResourceFile();

	//------------------------------------------------------------------------------------------
	// Name: WriteFile
	// Author: EuaconLabs
	// Date: 19.07.2014 | 23:27
	//
	// Description: Reads all given files and combines them into a single RES-File.
	// Remarks: Returns True on success, or False on failure.
	//------------------------------------------------------------------------------------------
	static bool WriteFile(const char* file, const rsFileWriteInfo* files, const rsu32_t numFiles);

	//------------------------------------------------------------------------------------------
	// Name: Load
	// Author: EuaconLabs
	// Date: 19.07.2014 | 19:59
	//
	// Description: Load the given resource file.
	// Returns: True if the file could be loaded correctly. False if an error occured.
	//------------------------------------------------------------------------------------------
	bool Load(const char* file);

	//------------------------------------------------------------------------------------------
	// Name: OpenFile
	// Author: EuaconLabs
	// Date: 19.07.2014 | 22:47
	//
	// Description: Tries to find the given file. If the file was found it's raw data will be
	//				loaded into memory and a rsFileInfo will be returned. On failure 0 will be
	//				returned.
	// Remarks: The returned rsFileInfo has to be deleted manually.
	//------------------------------------------------------------------------------------------
	rsFileInfo* OpenFile(const char* file);

private:

	struct rsWriteState
	{
		std::ofstream& out;
		rsu32_t filechunk;
		const rsFileWriteInfo* file;
		rsFileTableEntry* entry;
	};

	//------------------------------------------------------------------------------------------
	// Name: WriteFileRaw
	// Author: EuaconLabs
	// Date: 20.07.2014 | 16:07
	//
	// Description: Writes a raw file to the given output stream.
	// Remarks:
	//------------------------------------------------------------------------------------------
	static bool WriteFileRaw(rsWriteState& state);
	//------------------------------------------------------------------------------------------
	// Name: WriteFileCompressed
	// Author: EuaconLabs
	// Date: 20.07.2014 | 16:24
	//
	// Description: Writes a file by loading it from the file system then compressing its
	//				contents and finally writing it into the output stream.
	// Remarks: This method uses ZLib for data compression.
	//------------------------------------------------------------------------------------------
	static bool WriteFileCompressed(rsWriteState& state);

	//------------------------------------------------------------------------------------------
	// Name: OpenFileRaw
	// Author: EuaconLabs
	// Date: 20.07.2014 | 17:28
	//
	// Description: Opens the given file assuming it is not compressed.
	// Remarks:
	//------------------------------------------------------------------------------------------
	rsFileInfo* OpenFileRaw(const rsFileHeader& header);
	//------------------------------------------------------------------------------------------
	// Name: OpenFileCompressede
	// Author: EuaconLabs
	// Date: 20.07.2014 | 17:36
	//
	// Description: Opens the given file assuming it has been compressed.
	// Remarks:
	//------------------------------------------------------------------------------------------
	rsFileInfo* OpenFileCompressed(const rsFileHeader& header);

	std::ifstream input; // Input stream

	rsBaseHeader fileHeader; // The file's file header.
	bool native; // Are bytes in native byte order?

	rsu32_t fileChunk; // Offset to the beginning of the file chunk in absolute bytes.

	rsFileTableEntryMem* fileTable; // The resource file's file table.
};

#endif /* RESFILE_H_ */
