/*
 * fileinfo.h
 *
 *  Created on: 19.07.2014
 *      Author: euaconlabs
 */

#ifndef FILEINFO_H_
#define FILEINFO_H_

#include "base.h"

//------------------------------------------------------------------------------------------
// Name: rsFileInfo
// Author: EuaconLabs
// Date: 19.07.2014 | 22:24
//
// Description: Holds information about a file stored in a RES-File.
// Remarks:
//------------------------------------------------------------------------------------------
class rsFileInfo
{
	friend class rsResourceFile;
public:

	rsFileInfo();
	~rsFileInfo();

	//------------------------------------------------------------------------------------------
	// Name: GetName
	// Author: EuaconLabs
	// Date: 19.07.2014 | 22:30
	//
	// Description: Returns the name of the file including its path.
	// Remarks:
	//------------------------------------------------------------------------------------------
	const char* GetName() const;

	//------------------------------------------------------------------------------------------
	// Name: GetSize
	// Author: EuaconLabs
	// Date: 19.07.2014 | 22:43
	//
	// Description: Gets the size of the file in bytes.
	// Remarks:
	//------------------------------------------------------------------------------------------
	rsu32_t GetSize() const;

	//------------------------------------------------------------------------------------------
	// Name: GetFlags
	// Author: EuaconLabs
	// Date: 19.07.2014 | 22:43
	//
	// Description: Gets the flags of the file.
	// Remarks:
	//------------------------------------------------------------------------------------------
	rsu32_t GetFlags() const;

	//------------------------------------------------------------------------------------------
	// Name: GetData
	// Author: EuaconLabs
	// Date: 19.07.2014 | 22:28
	//
	// Description: Returns a pointer to the completely raw file data.
	// Remarks:
	//------------------------------------------------------------------------------------------
	const void* GetData() const;

	//------------------------------------------------------------------------------------------
	// Name: IsCompressed
	// Author: EuaconLabs
	// Date: 20.07.2014 | 17:04
	//
	// Description: Checks whether this file is compressed or not.
	// Remarks:
	//------------------------------------------------------------------------------------------
	bool IsCompressed() const;

private:

	char name[RS_FILE_MAX_NAME_LEN + 1]; // The file's name.
	rsu32_t size;
	rsu32_t flags;

	rsu8_t* data;
};

#endif /* FILEINFO_H_ */
