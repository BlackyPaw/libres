/*
 * headers.cpp
 *
 *  Created on: 19.07.2014
 *      Author: euaconlabs
 */

#include "headers.h"

rsu32_t rsFileEndianness = 0x01020304;

#if defined(_MSC_VER)
/* MSVC */
#include <stdlib.h>

#define rsSwap16(x) _byteswap_ushort(x)
#define rsSwap32(x) _byteswap_ulong(x)
#define rsSwap64(x) _byteswap_uint64(x)
#elif defined(__GNUC__) || defined(__GNUG__)
/* GCC */

// __builtin_bswap16 might not work on some platforms:
//#define rsSwap16(x) __builtin_bswap16(x)
#define rsSwap16(x) ((x & 0xFF00) >> 8 | (x & 0x00FF) << 8)
#define rsSwap32(x) __builtin_bswap32(x)
#define rsSwap64(x) __builtin_bswap64(x)
#else
/* Workaround */
rsu16_t rsSwap16(rsu16_t x)
{
	return ((x & 0xFF00) >> 8 | (x & 0x00FF) << 8);
}
rsu32_t rsSwap32(rsu32_t x)
{
	return ((x & 0xFF000000) >> 24 | (x & 0x000000FF) << 24 |
			(x & 0x00FF0000) >>  8 | (x & 0x0000FF00) <<  8 );
}
rsu64_t rsSwap64(rsu64_t x)
{
	return ((x & 0xFF00000000000000) >> 56 | (x & 0x00000000000000FF) << 56 |
			(x & 0x00FF000000000000) >> 40 | (x & 0x000000000000FF00) << 40 |
			(x & 0x0000FF0000000000) >> 24 | (x & 0x0000000000FF0000) << 24 |
			(x & 0x000000FF00000000) >>  8 | (x & 0x00000000FF000000) <<  8 );
}
#endif

rsBaseHeader& rsSwap(rsBaseHeader& h)
{
	h.endian		= rsSwap32(h.endian);
	h.version.major = rsSwap16(h.version.major);
	h.version.minor = rsSwap16(h.version.minor);
	h.files			= rsSwap32(h.files);
	h.offset		= rsSwap32(h.offset);
	return h;
}

rsFileHeader& rsSwap(rsFileHeader& h)
{
	h.size			= rsSwap32(h.size);
	h.flags			= rsSwap32(h.flags);
	h.rawsize		= rsSwap32(h.rawsize);
	return h;
}

rsFileTableRecord& rsSwap(rsFileTableRecord& h)
{
	h.size			= rsSwap32(h.size);
	return h;
}

rsFileTableEntry& rsSwap(rsFileTableEntry& h)
{
	h.offset		= rsSwap32(h.offset);
	h.namelen		= rsSwap32(h.namelen);
	return h;
}
