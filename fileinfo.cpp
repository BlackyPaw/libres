/*
 * fileinfo.cpp
 *
 *  Created on: 19.07.2014
 *      Author: euaconlabs
 */

#include "fileinfo.h"
#include "resfile.h"

rsFileInfo::rsFileInfo() :
	size(0),
	flags(0),
	data(0)
{
	name[RS_FILE_MAX_NAME_LEN] = '\0';
}

rsFileInfo::~rsFileInfo()
{
	if(data != 0)
		delete[] data;
}

const char* rsFileInfo::GetName() const
{
	return name;
}

rsu32_t rsFileInfo::GetSize() const
{
	return size;
}

rsu32_t rsFileInfo::GetFlags() const
{
	return flags;
}

const void* rsFileInfo::GetData() const
{
	return data;
}

bool rsFileInfo::IsCompressed() const
{
	return (flags & rsFILE_COMPRESSED) != 0;
}
