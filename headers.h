/*
 * headers.h
 *
 *  Created on: 19.07.2014
 *      Author: euaconlabs
 */

#ifndef HEADERS_H_
#define HEADERS_H_

#include "base.h"

extern rsu32_t rsFileEndianness;

//------------------------------------------------------------------------------------------
// rsBaseHeader
// Base header of RES-Files.
//------------------------------------------------------------------------------------------
#pragma pack(push, 1)

struct rsBaseHeader
{
	char			signature[4];		// 'RES ' => 0x52455320
	rsu32_t			endian;				// 0x01020304
	rsTagVersion 	version;			// File version
	rsu32_t			files;				// Number of files
	rsu32_t			offset;				// Offset to the first file
};

#pragma pack(pop)

extern rsBaseHeader& rsSwap(rsBaseHeader& h);

//------------------------------------------------------------------------------------------
// rsFileHeader
// Information about a specific file stored right before its raw data.
//------------------------------------------------------------------------------------------
#pragma pack(push, 1)

struct rsFileHeader
{
	rsu32_t			size;				// Size of the file in bytes.
	rsu32_t			flags;				// Flags of the file.
	rsu32_t			rawsize;			// If the file is compressed this contains the uncompressed size.
};

#pragma pack(pop)

extern rsFileHeader& rsSwap(rsFileHeader& h);

//------------------------------------------------------------------------------------------
// rsFileTableRecord
// Information stored in the file table found at the end of a RES-File.
//------------------------------------------------------------------------------------------
#pragma pack(push, 1)

struct rsFileTableRecord
{
	rsu32_t			size;				// The size of the file table.
};

#pragma pack(pop)

extern rsFileTableRecord& rsSwap(rsFileTableRecord& h);

//------------------------------------------------------------------------------------------
// rsFileTableEntry
// Information about an entry found in the file table.
//------------------------------------------------------------------------------------------
#pragma pack(push, 1)

struct rsFileTableEntry
{
	rsu32_t			offset;				// The offset in bytes from the beginning of the (!) file chunk (!)
	rsu32_t			namelen;
};

#pragma pack(pop)

extern rsFileTableEntry& rsSwap(rsFileTableEntry& h);

//------------------------------------------------------------------------------------------
// rsFileTableEntryMem
// Complete file table entry also containing the file's name.
//------------------------------------------------------------------------------------------
struct rsFileTableEntryMem
{
	char			name[RS_FILE_MAX_NAME_LEN];
	rsu32_t			offset;
};

//------------------------------------------------------------------------------------------
// rsFileWriteInfo
// Information about a file to be stored in a RES-File.
//------------------------------------------------------------------------------------------
struct rsFileWriteInfo
{
public:
	rsFileWriteInfo() :
		flags(0)
	{
		name[RS_FILE_MAX_NAME_LEN] = '\0';
		file[1023] = '\0';
	}

	char			name[RS_FILE_MAX_NAME_LEN+1];
	char			file[1024];
	rsu32_t			flags;
};

#endif /* HEADERS_H_ */
