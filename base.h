/*
 * base.h
 *
 *  Created on: 19.07.2014
 *      Author: euaconlabs
 */

#ifndef BASE_H_
#define BASE_H_

/*
 * Commenting template:
 */
//------------------------------------------------------------------------------------------
// Name:
// Author:
// Date:
//
// Description:
// Remarks:
//------------------------------------------------------------------------------------------

#define RS_FILE_MAX_NAME_LEN 256

typedef unsigned char rsu8_t;
typedef   signed char rss8_t;

typedef unsigned short rsu16_t;
typedef   signed short rss16_t;

typedef unsigned int rsu32_t;
typedef   signed int rss32_t;

typedef unsigned long int rsu64_t;
typedef   signed long int rss64_t;

#pragma pack(push, 1)

struct rsTagVersion
{
	rsu16_t major, minor;
};

#pragma pack(pop)

#endif /* BASE_H_ */
